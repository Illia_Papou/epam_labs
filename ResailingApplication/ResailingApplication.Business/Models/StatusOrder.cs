﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class StatusOrder : GenericModel<StatusOrder>
    {
        public string Name { get; set; }
        public List<Order> Orders { get; set; }
    }
}
