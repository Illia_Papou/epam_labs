﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class Role : GenericModel<Role>
    {
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
