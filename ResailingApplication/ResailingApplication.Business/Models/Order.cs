﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class Order : GenericModel<Order>
    {
        public Ticket Ticket { get; set; }
        public StatusOrder StatusOrder { get; set; }
        public User Buyer { get; set; }
        public string TrackNO { get; set; }
    }
}
