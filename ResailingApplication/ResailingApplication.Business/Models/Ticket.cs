﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class Ticket : GenericModel<Ticket>
    {
        public Event Event { get; set; }
        public double Price { get; set; }
        public User Seller { get; set; }
        public SellerNote SellerNote { get; set; }
        public List<Order> Orders { get; set; }
    }
}
