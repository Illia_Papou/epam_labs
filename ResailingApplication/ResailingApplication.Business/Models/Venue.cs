﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class Venue : GenericModel<Venue>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public City City { get; set; }
        public List<Event> Events { get; set; }
    }
}
