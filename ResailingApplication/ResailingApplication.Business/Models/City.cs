﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class City : GenericModel<City>
    {
        public string Name { get; set; }
        public List<Venue> Venues { get; set; }
    }
}
