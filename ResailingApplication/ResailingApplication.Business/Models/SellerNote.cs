﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Business.Models
{
    public class SellerNote : GenericModel<SellerNote>
    {
        public string Name { get; set; }
        public List<Ticket> Tickets { get; set; }
    }
}
