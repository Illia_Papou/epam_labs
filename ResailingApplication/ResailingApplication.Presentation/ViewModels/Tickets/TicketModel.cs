﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Tickets
{
    public class TicketModel
    {
        public List<SelectListItem> Events { get; set; }

        // If update ticket 
        public int Id { get; set; }

        [Required(ErrorMessage = "The price field must not be empty")]
        public double Price { get; set; }

        [Required(ErrorMessage = "The description field must not be empty")]
        public string Description { get; set; }

        public int EventId { get; set; }
    }
}
