﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Tickets
{
    public class RejectModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The comment field must not be empty")]
        public string Comment { get; set; }
    }
}
