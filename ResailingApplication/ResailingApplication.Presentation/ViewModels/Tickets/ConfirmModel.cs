﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Tickets
{
    public class ConfirmModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The tracking number field must not be empty")]
        public string TrackingNumber { get; set; }
    }
}
