﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Presentation.ViewModels.Tickets
{
    public class TicketsModel
    {
        public Event Event { get; set; }
        public List<Ticket> Ticket { get; set; }

        public TicketsModel(Event events, IEnumerable<Ticket> ticket)
        {
            Event = events;
            Ticket = ticket.ToList<Ticket>();
            
        }
    }
}
