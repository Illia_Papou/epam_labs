﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;

namespace ResailingApplication.Presentation.ViewModels.Tickets
{
    public class MyTicketsModel
    {
        public List<Ticket> SellingTickets { get; set; }
        public List<Order> WaitingConfirmationOrders { get; set; }
        public List<Order> ConfirmedOrders { get; set; }

        public MyTicketsModel(User user, List<Ticket> ticket, List<Order> order)
        {
            SellingTickets = ticket.Where(tickets => tickets.Seller.Id == user.Id).ToList<Ticket>();
            WaitingConfirmationOrders = order.Where(tickets => tickets.Ticket.Seller.Id == user.Id && tickets.StatusOrder.Name == "Waiting for confirmation").ToList<Order>();
            ConfirmedOrders = order.Where(tickets => tickets.Ticket.Seller.Id == user.Id && tickets.StatusOrder.Name == "Confirmed").ToList<Order>();
        }
    }
}
