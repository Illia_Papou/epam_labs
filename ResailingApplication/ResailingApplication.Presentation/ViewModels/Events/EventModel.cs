﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Events
{
    public class EventModel
    {
        //private List<SelectListItem> _citiesSelectItems;
        private List<SelectListItem> _venuesSelectItems;
        //private List<City> _cities;
        private List<Venue> _venues;

       // [BindNever]
        public List<SelectListItem> Cities { get; set; }
        public List<SelectListItem> Venues { get; set; }
        public List<SelectListItem> Events { get; set; }

        //[BindNever]


        

        // If update event 
        public int Id { get; set; }

        [Required(ErrorMessage = "The venue select must not be empty")]
        public int VenueId { get; set; }

        [Required(ErrorMessage = "The name field must not be empty")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "The date field must not be empty")]
        public DateTime Date { get; set; }

        public string Banner { get; set; }

        [Required(ErrorMessage = "The description field must not be empty")]
        public string Description { get; set; }





        public List<SelectListItem> GetVenuesByCity(string cityId)
        {
            _venuesSelectItems = new List<SelectListItem>();
            foreach (Venue item in _venues)
            {
                if (!_venuesSelectItems.Exists(c => c.Value == item.Id.ToString()) && item.City.Id == Convert.ToInt32(cityId))
                {
                    _venuesSelectItems.Add(new SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
                }
            }
            return _venuesSelectItems;
        }
    }
}
