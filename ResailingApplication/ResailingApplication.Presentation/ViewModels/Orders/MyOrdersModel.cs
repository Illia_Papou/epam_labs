﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;

namespace ResailingApplication.Presentation.ViewModels.Orders
{
    public class MyOrdersModel
    {
        //public List<Ticket> SellingTickets { get; set; }
        //public List<Order> WaitingConfirmationOrders { get; set; }
        public List<Order> BuyingOrders { get; set; }

        public MyOrdersModel(User user, List<Order> order)
        {
            //SellingTickets = ticket.Where(tickets => tickets.Seller.Id == user.Id).ToList<Ticket>();
            //WaitingConfirmationOrders = order.Where(tickets => tickets.Ticket.Seller.Id == user.Id && tickets.StatusOrder.Name == "Waiting for confirmation").ToList<Order>();
            BuyingOrders = order.Where(tickets => tickets.Buyer.Id == user.Id).ToList<Order>();
        }
    }
}
