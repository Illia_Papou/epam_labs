﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Manager
{
    public class UserModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The First name field must not be empty")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The Last name field must not be empty")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The Localization field must not be empty")]
        public string Localization { get; set; }

        [Required(ErrorMessage = "The Address field must not be empty")]
        public string Address { get; set; }
    }
}
