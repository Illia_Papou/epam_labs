﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.ApiViewModels
{
    public class ApiEventModel: GenericModel<ApiEventModel>
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string City { get; set; }
        public string Venue { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
    }
}
