﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.ApiViewModels
{
    public class ApiCityModel: GenericModel<ApiCityModel>
    {
        public string Name { get; set; }
    }
}
