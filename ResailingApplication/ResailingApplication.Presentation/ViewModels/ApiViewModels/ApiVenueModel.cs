﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.ApiViewModels
{
    public class ApiVenueModel: GenericModel<ApiVenueModel>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
    }
}
