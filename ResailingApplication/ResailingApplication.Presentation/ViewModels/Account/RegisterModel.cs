﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Presentation.ViewModels.Account
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Login is null")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Password is null")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirming password is incorrect")]
        public string ConfirmPassword { get; set; }

       /* [Required(ErrorMessage = "Не указано имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Не указана фамилия")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Не указана локализация")]
        public string Localisation { get; set; }

        [Required(ErrorMessage = "Не указан адрес")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Не указан телефонный номер")]
        public string PhoneNumber { get; set; }*/
    }
}
