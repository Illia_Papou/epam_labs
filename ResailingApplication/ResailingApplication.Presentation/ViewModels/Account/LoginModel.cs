﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Presentation.ViewModels.Account
{
    public class LoginModel
    {

        [Required(ErrorMessage = "The login field must not be empty")]
        public string Login { get; set; }

        [Required(ErrorMessage = "The password field must not be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
       
    }
}
