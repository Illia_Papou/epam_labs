﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Venues
{
    public class VenueModel
    {
        //private List<SelectListItem> _citiesSelectItems;
        private List<City> _cities;

        [BindNever]
        public List<SelectListItem> Cities { get; set; }

        
        // If update venue 
        public int Id { get; set; }

        [Required(ErrorMessage = "The city select must not be empty")]
        public int CityId { get; set; }

        [Required(ErrorMessage = "The name field must not be empty")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The address field must not be empty")]
        public string Address { get; set; }




    }
}
