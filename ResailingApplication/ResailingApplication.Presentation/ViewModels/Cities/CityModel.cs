﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ViewModels.Cities
{
    public class CityModel
    {

        public List<SelectListItem> Countries = CountriesMvcList.Items;

        // If update city 
        public int Id { get; set; }

        [Required(ErrorMessage = "The country select must not be empty")]
        public string Country { get; set; }

        [Required(ErrorMessage = "The city field must not be empty")]
        public string City { get; set; }
    }
}
