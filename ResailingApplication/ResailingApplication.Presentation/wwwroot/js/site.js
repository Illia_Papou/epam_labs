﻿$(function () {
    var pagCurrentPage = 0;
    var pagPageSize = 0;
    var pagCount = 0;
    var arrJson = [];

    function initialCities(data, selector) {
        $.each(data, function (index, item) {
            $('<div>', {
                class: 'filter_ref'
            })
                .append($('<input/>', {
                    type: 'checkbox',
                    class: 's-cities_cb',
                    value: item.id
                }))
                .append($('<b/>').text(item.name))
                .appendTo(selector);
        });
    }

    function initialVenues(data, selector) {
        $.each(data, function (index, item) {
            $('<div>', {
                class: 'filter_ref'
            })
                .append($('<input/>', {
                    type: 'checkbox',
                    class: 's-venues_cb',
                    value: item.id
                }))
                .append($('<b/>').text((!item.name ? "" : item.name)))
                .appendTo(selector);
        });
    }

    function initialSessions(data, selector) {
        var rows = "";

        $.each(data, function (index, item) {
            var dates = moment(item.date, "YYYY-MM-DD HH:mm:ss");

            $('<tr/>')
                .append($('<td/>')
                    .append($('<div/>', { class: 'news_item' })
                     .append($('<div/>', { class: 'news_img_block' })
                        .append($('<img/>', { src: item.banner, alt: 'poster' })))))
               .append($('<td/>', { class: 'text-capitalize' }).text(item.name))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.city))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.venue))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.address))
                .append($('<td/>', { class: 'text-capitalize' })
                   .append($('<span/>').text(dates.format('DD') + '/' + dates.format('M') + '/' + dates.format('YYYY'))))
              .append($('<td/>', { class: 'text-capitalize' }).text(item.description))
                .appendTo(selector);
        });
    }


    (function getCities() {
        $.ajax({
            url: '/api/v1/cities',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",

            success: function (cities) {
                initialCities(cities, '.s-cities_block');
            }
        });
    })();

    (function getVenues() {
        $.ajax({
            url: '/api/v1/venues',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",

            success: function (venues) {
                initialVenues(venues, '.s-venues_block');
            }
        });
    })();

    (function getSessions() {
        $.ajax({
            url: '/api/v1/main',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",

            success: function (sessions, status, jqXHR) {
                records = [];
                                $(sessions).each(function (index, elem) {
                                        arrJson.push(elem.name);
                                    });
                initialSessions(sessions, '.tickets_list table');
                pagCurrentPage = parseInt(jqXHR.getResponseHeader('Paging-Offset'), 10);
                pagPageSize = parseInt(jqXHR.getResponseHeader('Paging-Limit'), 10);
                pagCount = parseInt(jqXHR.getResponseHeader('Paging-Total'), 10);
                $('#page-selection').bootpag({ total: calculatePageTotal() });
            }
        });
    })();

    function getRequest(currentTarget) {
        var request = '';

        var recordName = $('#s-autocomplete').val();

        var citiesCb = $('.s-cities_cb:checkbox:checked');
        var venuesCb = $('.s-venues_cb:checkbox:checked');

        var date = $('#s-daterange').val().replace(';', 'T');
        var dateFrom = date.split(' - ')[0];
        var dateTo = date.split(' - ')[1];

        var $currentTarget = $(currentTarget);
        var valueTarget = $currentTarget.attr('value');

        if (valueTarget) {
            if (valueTarget.toUpperCase() === 'ASC' || valueTarget.toUpperCase() === 'DESC') {
                var orderBy = $currentTarget.text();
                request += '&sort=' + valueTarget + '&orderBy=' + orderBy;

                if (valueTarget.toUpperCase() === 'ASC')
                    $currentTarget.attr('value', 'desc');
                else
                    $currentTarget.attr('value', 'asc');
            }
        }

        if (recordName) {
            request += '&partialName=' + recordName;
        }

        if (citiesCb) {
            $.each(citiesCb, function (index, item) {
                request += '&citiesIds=' + $(item).attr('value');
            });
        }
        if (venuesCb) {
            $.each(venuesCb, function (index, item) {
                request += '&venuesIds=' + $(item).attr('value');
            });
        }
        if (dateFrom) {
            request += '&fromDate=' + dateFrom;
        }
        if (dateTo) {
            request += '&toDate=' + dateTo;
        }
        if (pagCurrentPage) {
            request += '&page=' + pagCurrentPage;
        }
        return request.substring(1);
    }

    function sendRequest(request) {
        $.ajax({
            url: window.location.origin + '/api/v1/events?' + request,
            type: 'get',
            beforeSend: function () {
                $('.tickets_list table td').remove();
            },
            complete: function () {
            },
            success: function (sessions, status, jqXHR) {

                pagCurrentPage = parseInt(jqXHR.getResponseHeader('Paging-Offset'), 10);
                pagPageSize = parseInt(jqXHR.getResponseHeader('Paging-Limit'), 10);
                pagCount = parseInt(jqXHR.getResponseHeader('Paging-Total'), 10);
                $('#page-selection').bootpag({ total: calculatePageTotal() });
                initialSessions(sessions, '.tickets_list table');
            }
        });
    }

    $('#s-autocomplete').on('blur', function () {
        var request = getRequest();
        sendRequest(request);
    });
    $(document).on('change', ".s-cities_cb", function () {
        var request = getRequest();
        sendRequest(request);
    });
    $(document).on('change', ".s-venues_cb", function () {
        var request = getRequest();
        sendRequest(request);
    });
    $('#s-daterange').on('focus', function () {
        $(this).daterangepicker({
            locale: {
                "format": 'YYYY-MM-DD'
            },
            startDate: Date.now,
            endDate: Date.now
        });

        $(this).on('apply.daterangepicker', function () {
            var request = getRequest();
            sendRequest(request);
        });
        $(this).on('cancel.daterangepicker', function () {
            $(this).attr('value', '');
            $(this).val('');

            var request = getRequest();
            sendRequest(request);
        });
    });
    $('#s-name_sort').on('click', function () {
        var request = getRequest(this);
        sendRequest(request);
    });
    $('#s-date_sort').on('click', function () {
        var request = getRequest(this);
        sendRequest(request);
    });
    $('#page-selection').bootpag({
        total: 0,
        maxVisible: 3
    }).on("page", function (event, num) {
        pagCurrentPage = num;

        var request = getRequest(this);
        sendRequest(request);
    });

    $("#s-autocomplete").autocomplete({
                    source: arrJson
                });

    function calculatePageTotal() {
        return Math.ceil(pagCount / pagPageSize);
    }

});