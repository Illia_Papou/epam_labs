﻿using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ViewModels.ApiViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ApiServices
{
    public class ApiEventServices
    {
        public ApiEventServices()
        {
        }

        public IEnumerable<Event> FindByName(IEnumerable<Event> items, string partialName)
        {
            if (partialName != "" && partialName != null && items != null)
            {
                IEnumerable<Event> resultOfSearch = null;
                resultOfSearch = FindBy(items, e => e.Name.ToUpper().Contains(partialName.ToUpper()));
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<Event> FindByDateRange(IEnumerable<Event> items, DateTime fromDate, DateTime toDate)
        {
            if (fromDate.Date == DateTime.MinValue.Date)
            {
                fromDate = DateTime.Now;
            }
            if (toDate.Date == DateTime.MinValue.Date)
            {
                toDate = DateTime.MaxValue;
            }
            if (fromDate.Date >= DateTime.Now.Date && fromDate.Date < toDate.Date && items != null)
            {
                IEnumerable<Event> resultOfSearch = null;
                resultOfSearch = FindBy(items, e => e.Date.Date >= fromDate.Date && e.Date.Date <= toDate.Date);
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<Event> FindByCities(IEnumerable<Event> items, List<int> citiesIds)
        {
            if (citiesIds.Any() && items != null)
            {
                IEnumerable<Event> resultOfSearch = Enumerable.Empty<Event>();
                foreach (int cityId in citiesIds)
                {
                    resultOfSearch = resultOfSearch.Concat(FindBy(items, e => e.Venue.City.Id == cityId));
                }
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<Event> FindByVenues(IEnumerable<Event> items, List<int> venuesIds)
        {
            if (venuesIds.Any() && items != null)
            {
                IEnumerable<Event> resultOfSearch = Enumerable.Empty<Event>();
                foreach (int venueId in venuesIds)
                {
                    resultOfSearch = resultOfSearch.Concat(FindBy(items, e => e.Venue.Id == venueId));
                }
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<Event> SortBy(IEnumerable<Event> items, string param)
        {
            if (items != null && items.Count() > 0)
            {
                if (param.ToUpper() == "DATE")
                {
                    return items.OrderBy(e => e.Date);
                }
                else if (param.ToUpper() == "NAME")
                {
                    return items.OrderBy(e => e.Name);
                }
                else return null;
            }
            else return items;
        }

        public IEnumerable<ApiEventModel> GetDatabase(IEnumerable<Event> items)
        {
            if (items != null)
            {
                IEnumerable<ApiEventModel> result = Enumerable.Empty<ApiEventModel>();
                foreach (Event item in items)
                {
                    result = result.Append(CreateFromModel(item));
                }
                if (result.Count() < 1) return null;
                else return result;
            }
            else return null;
        }

        private ApiEventModel CreateFromModel(Event item)
        {
            if (item == null) return null;
            ApiEventModel result = new ApiEventModel()
            {
                Id = item.Id,
                Name = item.Name,
                Banner = item.Banner,
                Date = item.Date,
                Description = item.Description,
                Venue = item.Venue.Name,
                City = item.Venue.City.Name,
                Address = item.Venue.Address
            };
            return result;
        }

        private IEnumerable<Event> FindBy(IEnumerable<Event> items, Func<Event, bool> predicate)
        {
            return items.Where(predicate);
        }
    }
}
