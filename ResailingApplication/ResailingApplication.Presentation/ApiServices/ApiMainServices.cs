﻿using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ViewModels.ApiViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ApiServices
{
    public class ApiMainServices
    {
        public ApiMainServices()
        {
        }

        public IEnumerable<Event> FindByName(IEnumerable<Event> items, string partialName)
        {
            if (partialName != "" && partialName != null && items != null)
            {
                IEnumerable<Event> resultOfSearch = null;
                resultOfSearch = FindBy(items, e => e.Name.ToUpper().Contains(partialName.ToUpper()));
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<ApiMainModel> GetFromDatabase(IEnumerable<Event> items)
        {
            if (items != null)
            {
                IEnumerable<ApiMainModel> result = Enumerable.Empty<ApiMainModel>();
                foreach (Event item in items)
                {
                    result = result.Append(CreateFromModel(item));
                }
                if (result.Count() < 1) return null;
                else return result;
            }
            else return null;
        }

        private ApiMainModel CreateFromModel(Event item)
        {
            if (item == null) return null;
            ApiMainModel result = new ApiMainModel()
            {
                Name = item.Name
            };
            return result;
        }

        private IEnumerable<Event> FindBy(IEnumerable<Event> items, Func<Event, bool> predicate)
        {
            return items.Where(predicate);
        }
    }
}
