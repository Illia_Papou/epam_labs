﻿using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ViewModels.ApiViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ApiServices
{
    public class ApiCityServices
    {
        public ApiCityServices()
        {
        }

        public IEnumerable<City> FindByName(IEnumerable<City> items, string partialName)
        {
            if (partialName != "" && partialName != null && items != null)
            {
                IEnumerable<City> resultOfSearch = null;
                resultOfSearch = FindBy(items, e => e.Name.ToUpper().Contains(partialName.ToUpper()));
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }    

        public IEnumerable<ApiCityModel> GetDatabase(IEnumerable<City> items)
        {
            if (items != null)
            {
                IEnumerable<ApiCityModel> result = Enumerable.Empty<ApiCityModel>();
                foreach (City item in items)
                {
                    result = result.Append(CreateFromModel(item));
                }
                if (result.Count() < 1) return null;
                else return result;
            }
            else return null;
        }

        private ApiCityModel CreateFromModel(City item)
        {
            if (item == null) return null;
            ApiCityModel result = new ApiCityModel()
            {
                Id = item.Id,
                Name = item.Name
            };
            return result;
        }

        private IEnumerable<City> FindBy(IEnumerable<City> items, Func<City, bool> predicate)
        {
            return items.Where(predicate);
        }
    }
}
