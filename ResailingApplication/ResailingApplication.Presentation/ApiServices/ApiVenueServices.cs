﻿using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ViewModels.ApiViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.ApiServices
{
    public class ApiVenueServices
    {
        public ApiVenueServices()
        {
        }

        public IEnumerable<Venue> FindByCities(IEnumerable<Venue> items, List<int> citiesIds)
        {
            if (citiesIds.Any() && items != null)
            {
                IEnumerable<Venue> resultOfSearch = Enumerable.Empty<Venue>();
                foreach (int cityId in citiesIds)
                {
                    resultOfSearch = resultOfSearch.Concat(FindBy(items, e => e.City.Id == cityId));
                }
                if (resultOfSearch.Count() < 1) return null;
                else return resultOfSearch;
            }
            else return items;
        }

        public IEnumerable<ApiVenueModel> GetFromDatabase(IEnumerable<Venue> items)
        {
            if (items != null)
            {
                IEnumerable<ApiVenueModel> result = Enumerable.Empty<ApiVenueModel>();
                foreach (Venue item in items)
                {
                    result = result.Append(CreateFromModel(item));
                }
                if (result.Count() < 1) return null;
                else return result;
            }
            else return null;
        }

        private ApiVenueModel CreateFromModel(Venue item)
        {
            if (item == null) return null;
            ApiVenueModel result = new ApiVenueModel()
            {
                Id = item.Id,
                Name = item.Name,
                Address = item.Address,
                CityId = item.City.Id
            };
            return result;
        }

        private IEnumerable<Venue> FindBy(IEnumerable<Venue> items, Func<Venue, bool> predicate)
        {
            return items.Where(predicate);
        }
    }
}
