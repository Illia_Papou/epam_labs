﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Presentation.ViewModels.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResailingApplication.Data.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Data.Services.Abstract;

namespace ResailingApplication.Presentation.Controllers
{
    public class MyTicketController : Controller
    {
        //private UnitOfWorkRepositories UnitOfWorkRepositories = UnitOfWorkRepositories.Instance;

        private IOrderService _orderService;
        private IUserService _userService;
        private ITicketService _ticketService;
        private IEventService _eventService;

        public MyTicketController(IOrderService orderService, IUserService userService, ITicketService ticketService, IEventService eventService)
        {
            _orderService=orderService;
            _userService = userService;
            _ticketService = ticketService;
            _eventService = eventService;
    }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult MyTickets()
        {
            MyTicketsModel myTicketsModel = new MyTicketsModel(
                _userService.GetAllUsers().FirstOrDefault(user => user.Login == HttpContext.User.Identity.Name),
                _ticketService.GetAllTickets(),
                _orderService.GetAllOrders());
            return View(myTicketsModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult MyOrders()
        {
            User user = _userService.GetAllUsers().FirstOrDefault(users =>
            users.Login == HttpContext.User.Identity.Name);
            List<Order> myOrders = _orderService.GetAllOrders().Where(orders => orders.Buyer.Id == user.Id).ToList<Order>();
            return View(myOrders);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            TicketModel model = new TicketModel();
            model.Events = new List<SelectListItem>();
            List<Event> _events = _eventService.GetAllEvents().ToList();
            foreach (Event item in _events)
            {
                model.Events.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name + ", " + item.Venue.City.Name +  item.Date.ToString("dd.MM.yyyy")
                });
            }
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TicketModel model)
        {
            if (ModelState.IsValid)
            {
                Ticket ticket = _ticketService.GetAllTickets()
                    .FirstOrDefault(t => t.Price == model.Price && t.Event.Id == model.EventId);
                if (ticket == null)
                {
                    SellerNote note = new SellerNote();
                    note.Name = model.Description;
                    //Add Ticket To storage
                    Event item = _eventService.GetEvent(model.EventId);
                    ticket = new Ticket
                    {
                        Price = model.Price,
                        SellerNote = note,
                        Event = item,
                        Seller = _userService.GetAllUsers()
                        .FirstOrDefault(u => u.Login == User.Identity.Name)
                    };

                    _ticketService.CreateTicket(ticket);
                    //_ticketService.Save();

                    return RedirectToAction("MyTickets", "MyTicket");
                }
                else
                    ModelState.AddModelError("", "A this ticket already exists");
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Confirm(int id)
        {
            ConfirmModel model = new ConfirmModel() { Id = id };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Confirm(ConfirmModel model)
        {
            if (ModelState.IsValid)
            {
                Order order = _orderService.GetOrdersById(model.Id);
                if (order != null)
                {
                    StatusOrder statusOrder = _orderService.GetAllStatusOrders().FirstOrDefault(s => s.Name == "Confirmed");
                    // Modificy order in storage
                    order.StatusOrder = statusOrder;
                    order.TrackNO = model.TrackingNumber;

                    _orderService.UpdateOrder(order);
                    //_orderService.Save();

                    return RedirectToAction("MyTickets", "MyTicket");
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Reject(int id)
        {
            RejectModel model = new RejectModel() { Id = id };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Reject(RejectModel model)
        {
            if (ModelState.IsValid)
            {
                Order order = _orderService.GetOrdersById(model.Id);
                if (order != null)
                {
                    StatusOrder statusOrder = _orderService.GetAllStatusOrders().FirstOrDefault(s => s.Name == "Rejected");
                    // Modificy order in storage
                    order.StatusOrder = statusOrder;

                    _orderService.UpdateOrder(order);
                    //_orderService.Save();

                    return RedirectToAction("MyTickets", "MyTicket");
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }
    }
}
