﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ApiServices;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ResailingApplication.Presentation.Controllers.Api
{
    [Route("api/v1/main")]
    public class ApiMainController : Controller
    {
        private IEventService _eventService;
        private ApiEventServices _apiEventService;

        public ApiMainController(IEventService eventService, ApiEventServices apiEventService)
        {
            _eventService = eventService;
            _apiEventService = apiEventService;
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept, [FromQuery]string term)
        {
            //Create fields
            const int limit = 10;
            IEnumerable<Event> items = _eventService.GetAllEvents();
            //Filtering process
            items = _apiEventService.FindByName(items, term);
            if (items == null)
                return NotFound();
            return new ObjectResult(_apiEventService.GetDatabase(items).Take(limit).ToList());
        }
    }
}
