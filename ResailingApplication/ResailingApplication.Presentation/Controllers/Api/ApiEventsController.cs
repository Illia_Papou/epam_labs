﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ApiServices;


namespace ResailingApplication.Presentation.Controllers.Api
{
    [Route("api/v1/events")]
    public class ApiEventsController : Controller
    {
        private IEventService _eventService;
        private ApiEventServices _apiEventServices;

        public ApiEventsController(IEventService eventService, ApiEventServices apiEventServices)
        {
            _eventService = eventService;
            _apiEventServices = apiEventServices;
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept,
            [FromQuery]string partialName, [FromQuery]DateTime fromDate,
            [FromQuery]DateTime toDate, [FromQuery]List<int> citiesIds,
            [FromQuery]List<int> venuesIds, [FromQuery]string sortBy = "DATE",
            [FromQuery]int limit = 3, [FromQuery]int offset = 1,
             [FromQuery]int page = 0)
        {
            //Create fields
            IEnumerable<Event> items = _eventService.GetAllEvents();
            //Translate
       
            //Filtering process
            //Step 1
            items = _apiEventServices.FindByName(items, partialName);
            if (items == null)
                return NotFound(partialName);
            //Step 2
            items = _apiEventServices.FindByDateRange(items, fromDate, toDate);
            if (items == null)
                return NotFound();
            //Step 3
            items = _apiEventServices.FindByCities(items, citiesIds);
            if (items == null)
                return NotFound();
            //Step 4
            items = _apiEventServices.FindByVenues(items, venuesIds);
            if (items == null)
                return NotFound();
            //Step 5
            items = _apiEventServices.SortBy(items, sortBy);
            if (items == null)
                return BadRequest();
            Response.Headers.Add("Paging-Total", items.Count().ToString());
            Response.Headers.Add("Paging-Limit", limit.ToString());
            Response.Headers.Add("Paging-Offset", offset.ToString());
            Response.Headers.Add("Paging-Returned", items.Skip(offset).Take(limit).Count().ToString());
            return new ObjectResult(_apiEventServices.GetDatabase(items).Skip((page-1)*limit).Take(limit).ToList()); //.Skip(offset).Take(limit).ToList()
            //
        }
    }
}
