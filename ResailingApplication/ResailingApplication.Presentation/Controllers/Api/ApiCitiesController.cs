﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ApiServices;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ResailingApplication.Presentation.Controllers.Api
{
    [Route("api/v1/cities")]
    public class ApiCitiesController : Controller
    {
        private IEventService _eventService;
        private ApiCityServices _apiCityService;

        public ApiCitiesController(IEventService eventService, ApiCityServices apiCityService)
        {
            _eventService = eventService;
            _apiCityService = apiCityService;
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept)
        {
            //Create fields
            IEnumerable<City> items = _eventService.GetAllCity();
            //Filtering process
            if (items.Count() < 1)
                return NotFound();
            //Translate
            foreach (City item in items)
            {
                item.Name = item.Name;
            }
            return new ObjectResult(_apiCityService.GetDatabase(items).ToList());
        }
    }
}
