﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ApiServices;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ResailingApplication.Presentation.Controllers.Api
{
    [Route("api/v1/venues")]
    public class ApiVenuesController : Controller
    {
        private IEventService _eventService;
        private ApiVenueServices _apiVenueService;

        public ApiVenuesController(IEventService eventService, ApiVenueServices apiVenueService)
        {
            _eventService = eventService;
            _apiVenueService = apiVenueService;
        }
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept, [FromQuery]List<int> citiesIds)
        {
            //Create fields
            IEnumerable<Venue> items = _eventService.GetAllVenue();
            //Filtering process
            items = _apiVenueService.FindByCities(items, citiesIds);
            if (items == null)
                return NotFound();
            //Translate
            foreach (Venue item in items)
            {
                item.Name = item.Name;
                item.Address = item.Address;
            }
            return new ObjectResult(_apiVenueService.GetFromDatabase(items).ToList());
        }
    }
}
