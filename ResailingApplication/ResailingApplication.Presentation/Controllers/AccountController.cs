﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using ResailingApplication.Presentation.ViewModels.Account;
using ResailingApplication.Business.Models;
using Microsoft.AspNetCore.Http;
//using ResailingApplication.Data.Repositories;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Data.Services;
using Microsoft.AspNetCore.Authorization;
using ResailingApplication.Data.Services.Abstract;

namespace ResailingApplication.Presentation.Controllers
{
    public class AccountController : Controller
    {

        private IUserService _userService;
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;

        public AccountController(IUserService userService, IUserRepository userRepository, IRoleRepository roleRepository)
        {
            _userService = userService;
            // _userService = userService;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                //Role userRole = _userService.GetAllRoles().FirstOrDefault(r => r.Name == "User");
                User user = _userService.GetAllUsers().FirstOrDefault(u => u.Login == model.Login);
                if (user == null)
                {
                    user = new User { Login = model.Login, Password = model.Password, Localization = "en" };
                    Role userRole = _userService.GetAllRoles().FirstOrDefault(r => r.Name == "User");
                    if (userRole != null)
                        user.Role = userRole;

                    _userService.CreateUser(user);
                    await Authenticate(user);

                    return RedirectToAction("Events", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _userService.GetAllUsers()
                    .FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(user);

                    return RedirectToAction("Events", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        private async Task Authenticate(User user)
        {
            List<Claim> claims;
            if (user.FirstName != null && user.LastName != null)
            {
                claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name),
                    new Claim("UserName", user.FirstName + " " + user.LastName),
                };
            }
            else
            {
                claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name),
                    new Claim("UserName", "NoName"),
                };
            }

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }

        public IActionResult UserInfo()
        {
            User user = _userService.GetAllUsers().FirstOrDefault(users =>
            users.Login == HttpContext.User.Identity.Name);
            return View(user);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult UsersRights()
        {
            return View(_userService.GetAllUsers().Where(u => u.Login != User.Identity.Name).ToList());
        }

        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRights(int id)
        {
            User selectedUser = _userService.GetUser(id);

            if (selectedUser.Role.Name == "User")
            {
                Role adminRole = _userService.GetAllRoles().FirstOrDefault(r => r.Name == "Admin");
                selectedUser.Role = adminRole;
            }
            else if (selectedUser.Role.Name == "Admin")
            {
                Role userRole = _userService.GetAllRoles().FirstOrDefault(r => r.Name == "User");
                selectedUser.Role = userRole;
            }

            _userService.UpdateUser(selectedUser);
            //_userService.Save();

            return RedirectToAction("UsersRights", "Account");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int id)
        {
            User selectedUser = _userService.GetUser(id);
            return View(selectedUser);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult DeleteConfirmed(int id)
        {
            _userService.DeleteUser(id);
            _userService.Save();

            return RedirectToAction("UsersRights", "Account");
        }

        [Authorize(Roles = "Admin")]
        public IActionResult AdminBar()
        {
            return View();
        }
    }
}
