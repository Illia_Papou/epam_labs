﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Business.Models;
using ResailingApplication.Data.Services;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Presentation.ViewModels.Cities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CitiesController : Controller
    {
        private IEventService _eventService;

        public CitiesController(IEventService eventService)
        {
            _eventService = eventService; ;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_eventService.GetAllCity());
        }

        [HttpGet]
        public IActionResult Create()
        {
            CityModel model = new CityModel();
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CityModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _eventService.GetAllCity().FirstOrDefault(c => c.Name == model.City);
                if (city == null)
                {
                    //Add City To storage
                    city = new City { Name = model.City};

                    _eventService.CreateCity(city);
                    //_eventService.Save();

                    return RedirectToAction("Index", "Cities");
                }
                else
                    ModelState.AddModelError("", "A this city already exists");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            City _selectedCity = _eventService.GetCity(id);
            CityModel model = new CityModel() { Id = id, City = _selectedCity.Name};
            foreach (SelectListItem item in model.Countries)
            {
                item.Text = item.Value;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(CityModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _eventService.GetCity(model.Id);
                if (city != null)
                {
                    //Add City To storage
                    city.Name = model.City;

                    _eventService.UpdateCity(city);
                    //_eventService.Save();

                    return RedirectToAction("Index", "Cities");
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            City selectedCity = _eventService.GetCity(id);
            return View(selectedCity);
        }

        public IActionResult DeleteConfirmed(int id)
        {
            _eventService.DeleteCity(id);
            //_eventService.Save();

            return RedirectToAction("Index", "Cities");
        }
    }
}
