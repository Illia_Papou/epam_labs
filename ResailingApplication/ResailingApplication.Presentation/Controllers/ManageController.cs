﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Business.Models;
using ResailingApplication.Data.Services;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Presentation.ViewModels.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Controllers
{
    public class ManageController : Controller
    {
        private IUserService _userService;
        private IHostingEnvironment _appEnvironment;

        public ManageController(IUserService userService, IHostingEnvironment appEnvironment)
        {
            _userService = userService;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Update(int id)
        {
            User _selectedUser = _userService.GetUser(id);
            UserModel model = new UserModel()
            {
                Id = id,
                FirstName = _selectedUser.FirstName,
                LastName = _selectedUser.LastName,
                Localization = _selectedUser.Localization,
                Address = _selectedUser.Address
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Update(UserModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _userService.GetUser(model.Id);
                if (user != null)
                {
                    //Add User To storage
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Localization = model.Localization;
                    user.Address = model.Address;

                    _userService.UpdateUser(user);
                    //_userService.Save();

                    return RedirectToAction("UserInfo", "Account");
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }        
    }
}
