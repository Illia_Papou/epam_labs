﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
//using ResailingApplication.Data.Repositories;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;
using ResailingApplication.Presentation.ViewModels.Tickets;

namespace ResailingApplication.Presentation.Controllers
{
    public class HomeController : Controller
    {
        //private UnitOfWorkRepositories UnitOfWorkRepositories = UnitOfWorkRepositories.Instance;      
        
        private IEventRepository eventRepository;
        private ITicketRepository ticketRepository;

        public HomeController(IEventRepository eventRepository, ITicketRepository ticketRepository)
        {
            this.eventRepository = eventRepository;
            this.ticketRepository = ticketRepository;
        }    
        public IActionResult Events()
        {
            return View(eventRepository.Event.ToList());
        }

        public IActionResult Tickets(int id)
        {
            Event events = eventRepository.Event.FirstOrDefault(eventt => eventt.Id == id);
            IEnumerable<Ticket> tickets = ticketRepository.Ticket.Where(ticket => ticket.Event.Id == events.Id);
            return View(new TicketsModel(events, tickets));
        }

        
    }
}
