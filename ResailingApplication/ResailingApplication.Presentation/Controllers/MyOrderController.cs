﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ResailingApplication.Business.Models;
using ResailingApplication.Data.Services;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Presentation.ViewModels.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Controllers
{
    public class MyOrderController : Controller
    {
        private IOrderService _orderService;
        private IUserService _userService;
        private ITicketService _ticketService;

        public MyOrderController(IOrderService orderService, IUserService userService, ITicketService ticketService)
        {
            _orderService = orderService;
            _userService = userService;
            _ticketService = ticketService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult MyOrders()
        {
            MyOrdersModel myOrdersModel = new MyOrdersModel( _userService.GetAllUsers().FirstOrDefault(item =>
            item.Login == HttpContext.User.Identity.Name),
            _orderService.GetAllOrders());
            return View(myOrdersModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create(int id)
        {
            Ticket ticket = _ticketService.GetTicketById(id);
            return View(ticket);
        }

        [Authorize]
        public IActionResult CreateConfirmed(int id)
        {
            Order order = new Order()
            {
                Ticket = _ticketService.GetTicketById(id),
                Buyer = _userService.GetAllUsers().FirstOrDefault(u => u.Login == User.Identity.Name),
                StatusOrder = new StatusOrder (){ Name = "Waiting for confirmation" },
                TrackNO = ""
            };

            _orderService.CreateOrder(order);
            //_orderService.Save();

            return RedirectToAction("MyOrders", "MyOrder");
        }

        public IActionResult ConfirmDelivery(int id)
        {
            Order order = _orderService.GetOrdersById(id);

            order.StatusOrder.Name = "Selling";

            _orderService.UpdateOrder(order);
            //_orderService.Save();

            return RedirectToAction("MyOrders", "MyOrder");
        }
    }
}
