﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Business.Models;
using ResailingApplication.Data.Services;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Presentation.ViewModels.Events;
using ResailingApplication.Presentation.ViewModels.Venues;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventsController : Controller
    {
        private IEventService _eventService;
        private IHostingEnvironment _appEnvironment;

        public EventsController(IEventService eventService, IHostingEnvironment appEnvironment)
        {
            _eventService = eventService;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_eventService.GetAllEvents());
        }

        [HttpGet]
        public IActionResult Create()
        {
            EventModel model = new EventModel();
            model.Cities = new List<SelectListItem>();
            model.Venues = new List<SelectListItem>();
            List<City> _cities = _eventService.GetAllCity().ToList();
            List<Venue> _venues = _eventService.GetAllVenue().ToList();
            foreach (City item in _cities)
            {
                model.Cities.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }
            foreach (Venue item in _venues)
            {
                model.Venues.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }
            /*foreach (SelectListItem item in model.Countries)
            {
                item.Text = item.Value;
            }*/
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EventModel model)
        {
            if (ModelState.IsValid)
            {
                Venue venue = _eventService.GetVenue(model.VenueId);
                Event item = _eventService.GetAllEvents().FirstOrDefault(e => e.Name == model.Name && e.Date == model.Date && e.Venue.Id == venue.Id);
                if (item == null)
                {
                    //Add Event To storage
                    item = new Event { Name = model.Name, Date = model.Date, Description = model.Description, Venue = venue };

                    _eventService.CreateEvent(item);
                    //_eventService.Save();

                    int eventId = _eventService.GetAllEvents().FirstOrDefault(e => e.Name == model.Name && e.Date == model.Date && e.Venue.Id == venue.Id).Id;

                    return RedirectToAction("ImageUpload", "Events", new { eventId = eventId });
                }
                else
                    ModelState.AddModelError("", "A this event already exists");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            Event _selectedEvent = _eventService.GetEvent(id);
            EventModel model = new EventModel() { Id = id, Name = _selectedEvent.Name, Date = _selectedEvent.Date, Description = _selectedEvent.Description, VenueId = _selectedEvent.Venue.Id };
            model.Cities = new List<SelectListItem>();
            model.Venues = new List<SelectListItem>();
            /*foreach (SelectListItem item in model.Countries)
            {
                item.Text = item.Value;
            }
            return View(model);*/

            List<City> _cities = _eventService.GetAllCity().ToList();
            List<Venue> _venues = _eventService.GetAllVenue().ToList();
            foreach (City item in _cities)
            {
                model.Cities.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }
            foreach (Venue item in _venues)
            {
                model.Venues.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(EventModel model)
        {
            if (ModelState.IsValid)
            {
                Venue venue = _eventService.GetVenue(model.VenueId);
                Event item = _eventService.GetEvent(model.Id);
                if (venue != null && item != null)
                {
                    //Add Event To storage
                    item.Name = model.Name;
                    item.Date = model.Date;
                    item.Description = model.Description;
                    item.Venue = venue;

                    _eventService.UpdateEvent(item);
                    //_eventService.Save();

                    return RedirectToAction("ImageUpload", "Events", new { eventId = item.Id });
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            Event selectedEvent = _eventService.GetEvent(id);
            return View(selectedEvent);
        }

        public IActionResult DeleteConfirmed(int id)
        {
            _eventService.DeleteEvent(id);
            //_eventService.Save();

            return RedirectToAction("Index", "Events");
        }

        [HttpGet]
        public IActionResult ImageUpload(int eventId)
        {
            ViewBag.EventId = eventId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ImageUpload(IFormFile uploadedFile, int eventId)
        {
            if (uploadedFile != null)
            {
                Event item = _eventService.GetEvent(eventId);
                string[] splitRoot = uploadedFile.FileName.Split('\\');
                item.Banner = "/images/" + splitRoot[splitRoot.Length-1];
                _eventService.UpdateEvent(item);
                //_eventService.Save();
                string root = uploadedFile.FileName;

                Directory.CreateDirectory(Path.GetDirectoryName(_appEnvironment.WebRootPath + "/images/"));
                using (var fileStream = new FileStream(root, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
            }
            return RedirectToAction("Index", "Events");
        }
             

        [HttpPost]
        public JsonResult GetVenues(string cityId)
        {
            EventModel model = new EventModel();
            model.Venues = new List<SelectListItem>();
            List<SelectListItem> venues = model.GetVenuesByCity(cityId);
            return Json(venues);
        }

        public IActionResult FullSearch()
        {
            return View();
        }
    }
}
