﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ResailingApplication.Business.Models;
using ResailingApplication.Data.Services;
using ResailingApplication.Data.Services.Abstract;
using ResailingApplication.Presentation.ViewModels.Venues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VenuesController : Controller
    {
        private IEventService _eventService;

        public VenuesController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_eventService.GetAllVenue());
        }

        [HttpGet]
        public IActionResult Create()
        {
            VenueModel model = new VenueModel();
            //model.Cities = _eventService.GetAllCity().ToList();
            model.Cities = new List<SelectListItem>();
            List<City> _cities = _eventService.GetAllCity().ToList();
            foreach (City item in _cities)
            {
                model.Cities.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(VenueModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _eventService.GetCity(model.CityId);
                Venue venue = _eventService.GetAllVenue().FirstOrDefault(v => v.Name == model.Name && v.City.Id == city.Id);
                if (venue == null)
                {
                    //Add Venue To storage
                    venue = new Venue { Name = model.Name, Address = model.Address, City = city };

                    _eventService.CreateVenue(venue);
                    //_eventService.Save();

                    return RedirectToAction("Index", "Venues");
                }
                else
                    ModelState.AddModelError("", "A this venue already exists");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            Venue _selectedVenue = _eventService.GetVenue(id);
            VenueModel model = new VenueModel() { Id = id, Name = _selectedVenue.Name, Address = _selectedVenue.Address, CityId = _selectedVenue.City.Id };
            model.Cities = new List<SelectListItem>();
            List<City> _cities = _eventService.GetAllCity().ToList();
            foreach (City item in _cities)
            {
                model.Cities.Add(new SelectListItem()
                {
                    Value = item.Id.ToString(),
                    Text = item.Name
                });
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(VenueModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _eventService.GetCity(model.CityId);
                Venue venue = _eventService.GetVenue(model.Id);
                if (venue != null && city != null)
                {
                    //Add Venue To storage
                    venue.Name = model.Name;
                    venue.Address = model.Address;
                    venue.City = city;

                    _eventService.UpdateVenue(venue);
                    //_eventService.Save();

                    return RedirectToAction("Index", "Venues");
                }
                else
                    ModelState.AddModelError("", "Error with Id or storage - retry later.");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            Venue selectedVenue = _eventService.GetVenue(id);
            return View(selectedVenue);
        }

        public IActionResult DeleteConfirmed(int id)
        {
            _eventService.DeleteVenue(id);
            //_eventService.Save();

            return RedirectToAction("Index", "Venues");
        }

        
    }
}
