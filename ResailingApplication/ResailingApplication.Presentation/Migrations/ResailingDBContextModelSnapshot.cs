﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ResailingApplication.Data.DataBase;

namespace ResailingApplication.Presentation.Migrations
{
    [DbContext(typeof(ResailingDBContext))]
    partial class ResailingDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ResailingApplication.Business.Models.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int?>("VenueId");

                    b.HasKey("Id");

                    b.HasIndex("VenueId");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BuyerId");

                    b.Property<int?>("StatusOrderId");

                    b.Property<int?>("TicketId");

                    b.Property<string>("TrackNO");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("StatusOrderId");

                    b.HasIndex("TicketId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.SellerNote", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("SellerNotes");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.StatusOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("StatusOrders");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EventId");

                    b.Property<double>("Price");

                    b.Property<int?>("SellerId");

                    b.Property<int?>("SellerNoteId");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("SellerId");

                    b.HasIndex("SellerNoteId");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Login");

                    b.Property<string>("Password");

                    b.Property<string>("PhoneNumber");

                    b.Property<int?>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Venue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int?>("CityId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Event", b =>
                {
                    b.HasOne("ResailingApplication.Business.Models.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Order", b =>
                {
                    b.HasOne("ResailingApplication.Business.Models.User", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerId");

                    b.HasOne("ResailingApplication.Business.Models.StatusOrder", "StatusOrder")
                        .WithMany("Orders")
                        .HasForeignKey("StatusOrderId");

                    b.HasOne("ResailingApplication.Business.Models.Ticket", "Ticket")
                        .WithMany("Orders")
                        .HasForeignKey("TicketId");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Ticket", b =>
                {
                    b.HasOne("ResailingApplication.Business.Models.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventId");

                    b.HasOne("ResailingApplication.Business.Models.User", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerId");

                    b.HasOne("ResailingApplication.Business.Models.SellerNote", "SellerNote")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerNoteId");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.User", b =>
                {
                    b.HasOne("ResailingApplication.Business.Models.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("ResailingApplication.Business.Models.Venue", b =>
                {
                    b.HasOne("ResailingApplication.Business.Models.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityId");
                });
        }
    }
}
