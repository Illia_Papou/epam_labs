﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ResailingApplication.Presentation.Resources.Formatters
{
    public class CsvFormat : OutputFormatter
    {
        private readonly CsvSettings _options;

        public string ContentType { get; private set; }

        public CsvFormat(CsvSettings csvFormatterOptions)
        {
            ContentType = "application/csv";
            SupportedMediaTypes.Add(Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/csv"));

            if (csvFormatterOptions == null)
            {
                throw new ArgumentNullException(nameof(csvFormatterOptions));
            }

            _options = csvFormatterOptions;
        }

        protected override bool CanWriteType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            return isTypeOfIEnumerable(type);
        }

        private bool isTypeOfIEnumerable(Type type)
        {
            foreach (Type interfaceType in type.GetInterfaces())
            {
                if (interfaceType == typeof(IList))
                    return true;
            }
            return false;
        }

        public async override Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            var response = context.HttpContext.Response;

            Type type = context.Object.GetType();
            Type itemType;

            if (type.GetGenericArguments().Length > 0)
            {
                itemType = type.GetGenericArguments()[0];
            }
            else
            {
                itemType = type.GetElementType();
            }

            StringWriter _stringWriter = new StringWriter();

            //Write header of csv format
            if (_options.UseSingleLineHeaderInCsv)
            {
                _stringWriter.WriteLine(
                    string.Join<string>(_options.CsvDelimiter, itemType.GetProperties().Select(x => x.Name))
                );
            }

            //Write body of csv format
            foreach (var obj in (IEnumerable<object>)context.Object)
            {
                var values = obj.GetType().GetProperties().Select(
                    property => new {
                        Value = property.GetValue(obj, null)
                    }
                );

                string _valueLine = string.Empty;

                foreach (var value in values)
                {
                    if (value.Value != null)
                    {
                        var _value = value.Value.ToString();

                        if (_value.Contains(","))
                            _value = string.Concat("\"", _value, "\"");
                        if (_value.Contains("\r"))
                            _value = _value.Replace("\r", " ");
                        if (_value.Contains("\n"))
                            _value = _value.Replace("\n", " ");

                        _valueLine = string.Concat(_valueLine, _value, _options.CsvDelimiter);
                    }
                    else
                    {
                        _valueLine = string.Concat(string.Empty, _options.CsvDelimiter);
                    }
                }

                _stringWriter.WriteLine(_valueLine.TrimEnd(_options.CsvDelimiter.ToCharArray()));
            }

            var streamWriter = new StreamWriter(response.Body);
            await streamWriter.WriteAsync(_stringWriter.ToString());
            await streamWriter.FlushAsync();
        }
    }
}
