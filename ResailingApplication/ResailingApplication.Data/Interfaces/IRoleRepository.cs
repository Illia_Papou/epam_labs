﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface IRoleRepository
    {
        IEnumerable<Role> Role { get; }
        void Adding(Role role);
        void Removing(Role role);
        void Updating(int index, Role role);
    }
}
