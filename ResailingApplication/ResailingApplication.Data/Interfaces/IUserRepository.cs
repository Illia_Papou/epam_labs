﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<User> User { get; }
        void Adding(User user);
        void Removing(User user);
        void Updating(int index, User user);
    }
}
