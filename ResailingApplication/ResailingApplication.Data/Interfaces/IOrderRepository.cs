﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{

    public interface IOrderRepository
    {
        IEnumerable<Order> Order { get; }
        void Adding(Order order);
        void Removing(Order order);
        void Updating(int index, Order order);
    }
}
