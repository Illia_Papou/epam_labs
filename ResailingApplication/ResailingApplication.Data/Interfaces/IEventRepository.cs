﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface IEventRepository
    {
        IEnumerable<Event> Event { get; }
        void Adding(Event events);
        void Removing(Event events);
        void Updating(int index, Event events);
    }
}
