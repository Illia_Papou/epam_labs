﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> City { get; }
        void Adding(City city);
        void Removing(City city);
        void Updating(int index, City city);
    }
}
