﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface ITicketRepository
    {
        IEnumerable<Ticket> Ticket { get; }
        void Adding(Ticket ticket);
        void Removing(Ticket ticket);
        void Updating(int index, Ticket ticket);
    }
}
