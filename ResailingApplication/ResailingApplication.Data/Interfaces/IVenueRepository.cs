﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface IVenueRepository
    {
        IEnumerable<Venue> Venue { get; }
        void Adding(Venue venue);
        void Removing(Venue venue);
        void Updating(int index, Venue venue);
    }
}
