﻿using ResailingApplication.Business.Models;
//using ResailingApplication.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Interfaces
{
    public interface ISellerNoteRepository
    {
        IEnumerable<SellerNote> SellerNote { get; }
    }
}
