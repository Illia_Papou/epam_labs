﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class EFDBInitializer
    {
        public static void Initialize(ResailingDBContext context)
        {
            context.Database.EnsureCreated();

            if (context.Cities.Any())
            {
                return;
            }

            //StatusOrders
            var statusWaiting = new StatusOrder
            {
                Name = "Waiting for confirmation"
            };
            var statusConfirmed = new StatusOrder
            {
                Name = "Confirmed"
            };
            var statusRejected = new StatusOrder
            {
                Name = "Rejected"
            };

            var statusOrders = new List<StatusOrder>
            {
                statusWaiting,
                statusConfirmed,
                statusRejected
            };
            context.StatusOrders.AddRange(statusOrders);
            context.SaveChanges();

            var vip = new SellerNote
            {
                Name = "VIP"
            };
            var smallHall = new SellerNote
            {
                Name = "Small Hall"
            };
            var majorHall = new SellerNote
            {
                Name = "Major Hall"
            };

            var sellerNotes = new List<SellerNote>
            {
                vip,
                smallHall,
                majorHall
            };

            context.SellerNotes.AddRange(sellerNotes);
            context.SaveChanges();

            // Cities
            var minsk = new City { Name = "Minsk" };
            var gomel = new City { Name = "Gomel" };
            var vitebsk = new City { Name = "Vitebsk" };
            var grodno = new City { Name = "Grodno" };
            var brest = new City { Name = "Brest" };
            var mogilew = new City { Name = "Mogilew" };

            var cities = new List<City>
            {
                minsk,
                gomel,
                vitebsk,
                grodno,
                brest,
                mogilew
            };
            context.Cities.AddRange(cities);
            context.SaveChanges();

            //Venues
            var redStar = new Venue
            {
                Name = "Cinema 'Red Star'",
                Address = "Socialisticheskaya Street 4",
                City = grodno
            };
            var oktyabr = new Venue
            {
                Name = "Cinema 'Octyabr'",
                Address = "Popovicha Street 3",
                City = grodno
            };
            var kalinina = new Venue
            {
                Name = "Cinema of Kalinina",
                Address = "Kommunarov Street 4",
                City = gomel
            };
            var peace = new Venue
            {
                Name = "Cinema 'Peace'",
                Address = "Il'icha Street 51b",
                City = gomel
            };
            var berestye = new Venue
            {
                Name = "Cinema 'Berestye'",
                Address = "avenue Paper 'Pravda' 25",
                City = minsk
            };
            var avrora = new Venue
            {
                Name = "Cinema 'Avrora'",
                Address = "Pritytsky Street 23",
                City = minsk
            };
            var oktiabr = new Venue
            {
                Name = "Cinema 'Octyabr'",
                Address = "avenue Peace 23",
                City = mogilew
            };
            var vetraz = new Venue
            {
                Name = "Cinema 'Vetraz'",
                Address = "Nikolay Ostrovsky Street 1",
                City = mogilew
            };
            var belarus = new Venue
            {
                Name = "Cinema 'Belarus'",
                Address = "Sovetskaya Street 62",
                City = brest
            };
            var peacebr = new Venue
            {
                Name = "Cinema 'Peace'",
                Address = "Pushkinskaya Street 27",
                City = brest
            };
            var homecinema = new Venue
            {
                Name = "Home of Cinema",
                Address = "Lenina Street 40",
                City = vitebsk
            };
            var peacevit = new Venue
            {
                Name = "Cinema 'Peace'",
                Address = "Chehova Street 3",
                City = vitebsk
            };
            var venues = new List<Venue>()
            {
                redStar,
                oktyabr,
                kalinina,
                peace,
                berestye,
                avrora,
                oktiabr,
                vetraz,
                belarus,
                peacebr,
                homecinema,
                peacevit
            };
            context.Venues.AddRange(venues);
            context.SaveChanges();

            //Roles
            var admin = new Role { Name = "Admin" };
            var user = new Role { Name = "User" };

            var roles = new List<Role>()
            {
                admin,
                user
            };
            context.Roles.AddRange(roles);
            context.SaveChanges();

            //Users
            var adminUser = new User
            {
                Login = "Admin",
                Password = "Admin",
                FirstName = "Illia",
                LastName = "Papou",
                Localization = "be",
                Address = "34 Makaenka 9th Street, Gomel, Belarus",
                PhoneNumber = "8(029)238-83-53",
                Role = admin
            };
            var user1 = new User
            {
                Login = "User",
                Password = "User",
                FirstName = "Max",
                LastName = "Potapov",
                Localization = "ru",
                Address = "35 Makaenka 9th Street, Gomel, Belarus",
                PhoneNumber = "8(029)228-83-53",
                Role = user
            };
            var user2 = new User
            {
                Login = "User2",
                Password = "User2",
                FirstName = "Bill",
                LastName = "Gates",
                Localization = "en",
                Address = "7400 Northeast 18th Street, Medina, Washington, United States",
                PhoneNumber = "(206) 709-3100",
                Role = user
            };

            var users = new List<User>()
            {
                adminUser,
                user1,
                user2
            };
            context.Users.AddRange(users);
            context.SaveChanges();

            //Events
            var transformers = new Event
            {
                Name = "Transformers: The Last Knight",
                Date = new DateTime(2017, 07, 15),
                Venue = berestye,
                Banner = "/images/Transformers_The_Last_Knight.jpg",
                Description = "Transformers: The Last Knight is an upcoming 2017 American science fiction-action film based on the Transformers toy line."

            };
            var apes = new Event
            {
                Name = "War for the Planet of the Apes",
                Date = new DateTime(2017, 10, 01),
                Venue = kalinina,
                Banner = "/images/War_for_the_Planet_of_the_Apes.jpg",
                Description = "War for the Planet of the Apes is an upcoming American science fiction film directed by Matt Reeves and written by Mark Bomback and Reeves. It is a sequel to the 2014 film Dawn of the Planet of the Apes and the third installment in the Planet of the Apes reboot series."

            };
            var lego = new Event
            {
                Name = "The Lego Batman Movie",
                Date = new DateTime(2017, 01, 25),
                Venue = avrora,
                Banner = "/images/The_Lego_Batman_Movie.jpg",
                Description = "The Lego Batman Movie is an upcoming 2017 American-Danish 3D computer-animated action-comedy superhero film. It is a spin-off of the 2014 film The Lego Movie, focusing on the DC Comics character Batman, set in the same universe as the previous film."

            };
            var justice = new Event
            {
                Name = "Justice League",
                Date = new DateTime(2017, 12, 28),
                Venue = redStar,
                Banner = "/images/Justice_League.jpg",
                Description = "Justice League is an upcoming American superhero film based on the DC Comics superhero team of the same name, distributed by Warner Bros. Pictures. It is intended to be the fifth installment in the DC Extended Universe."

            };
            var pirates = new Event
            {
                Name = "Pirates of the Caribbean: Dead Men Tell No Tales",
                Date = new DateTime(2017, 05, 25),
                Venue = belarus,
                Banner = "/images/Pirates_of_the_Caribbean,_Dead_Men_Tell_No_Tales.jpg",
                Description = "Pirates of the Caribbean: Dead Men Tell No Tales is an upcoming American fantasy swashbuckler film, and the fifth installment in the Pirates of the Caribbean film series. The film is directed by Joachim Rønning and Espen Sandberg from a script by Jeff Nathanson, with Jerry Bruckheimer serving again as producer."

            };
            var fast8 = new Event
            {
                Name = "Fast 8",
                Date = new DateTime(2017, 07, 28),
                Venue = oktyabr,
                Banner = "/images/F8.jpg",
                Description = "The Fate of the Furious is an upcoming 2017 American action film directed by F. Gary Gray and written by Chris Morgan. It is the eighth installment in The Fast and the Furious franchise."

            };

            var events = new List<Event>()
            {
                transformers,
                apes,
                lego,
                justice,
                pirates,
                fast8
            };
            context.Events.AddRange(events);
            context.SaveChanges();

            //Tickets
            var ticket1 = new Ticket
            {
                Event = apes,
                Price = 6.50,
                Seller = user2,
                SellerNote = vip
            };
            var ticket2 = new Ticket
            {
                Event = transformers,
                Price = 5.50,
                Seller = adminUser,
                SellerNote = majorHall

            };
            var ticket3 = new Ticket
            {
                Event = lego,
                Price = 4.50,
                Seller = user2,
                SellerNote = smallHall
            };
            var ticket4 = new Ticket
            {
                Event = apes,
                Price = 7.50,
                Seller = user2,
                SellerNote = vip
            };
            var ticket5 = new Ticket
            {
                Event = apes,
                Price = 5.00,
                Seller = adminUser,
                SellerNote = smallHall
            };
            var ticket6 = new Ticket
            {
                Event = lego,
                Price = 3.50,
                Seller = user1,
                SellerNote = smallHall
            };
            var ticket7 = new Ticket
            {
                Event = pirates,
                Price = 8.50,
                Seller = adminUser,
                SellerNote = vip
            };
            var ticket8 = new Ticket
            {
                Event = fast8,
                Price = 7.50,
                Seller = adminUser,
                SellerNote = vip
            };
            var ticket9 = new Ticket
            {
                Event = lego,
                Price = 6.50,
                Seller = user2,
                SellerNote = majorHall
            };
            var ticket10 = new Ticket
            {
                Event = transformers,
                Price = 6.00,
                Seller = user1,
                SellerNote = majorHall
            };

            var tickets = new List<Ticket>()
            {
                ticket1,
                ticket2,
                ticket3,
                ticket4,
                ticket5,
                ticket6,
                ticket7,
                ticket8,
                ticket9,
                ticket10
            };
            context.Tickets.AddRange(tickets);
            context.SaveChanges();

            //Orders
            var order1 = new Order
            {
                Ticket = ticket1,
                StatusOrder = statusWaiting,
                Buyer = adminUser,
                TrackNO = "EE12345678911"
            };
            var order2 = new Order
            {
                Ticket = ticket8,
                StatusOrder = statusWaiting,
                Buyer = adminUser,
                TrackNO = "EE12345678912"
            };
            var order3 = new Order
            {
                Ticket = ticket8,
                StatusOrder = statusWaiting,
                Buyer = user2,
                TrackNO = "EE12345678913"
            };
            var order4 = new Order
            {
                Ticket = ticket3,
                StatusOrder = statusRejected,
                Buyer = adminUser,
                TrackNO = "EE12345678914"
            };
            var order5 = new Order
            {
                Ticket = ticket2,
                StatusOrder = statusConfirmed,
                Buyer = user2,
                TrackNO = "EE12345678915"
            };
            var order6 = new Order
            {
                Ticket = ticket9,
                StatusOrder = statusConfirmed,
                Buyer = user1,
                TrackNO = "EE12345678916"
            };
            var order7 = new Order
            {
                Ticket = ticket2,
                StatusOrder = statusWaiting,
                Buyer = adminUser,
                TrackNO = "EE12345678917"
            };
            var order8 = new Order
            {
                Ticket = ticket8,
                StatusOrder = statusRejected,
                Buyer = adminUser,
                TrackNO = "EE12345678918"
            };
            var order9 = new Order
            {
                Ticket = ticket1,
                StatusOrder = statusRejected,
                Buyer = user2,
                TrackNO = "EE12345678919"
            };
            var order10 = new Order
            {
                Ticket = ticket7,
                StatusOrder = statusConfirmed,
                Buyer = user2,
                TrackNO = "EE12345678921"
            };

            var orders = new List<Order>()
            {
                order1,
                order2,
                order3,
                order4,
                order5,
                order6,
                order7,
                order8,
                order9,
                order10
            };
            context.AddRange(orders);
            context.SaveChanges();
        }
    }
}
