﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class StatusOrderRepository : IStatusOrderRepository
    {
        //ResailingDBContext context = new ResailingDBContext();
        private ResailingDBContext _context;

        public StatusOrderRepository()
        {
            _context = new ResailingDBContext();
        }

        public StatusOrderRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<StatusOrder> StatusOrder
        {
            get
            {
                return _context.StatusOrders;
            }
        }
    }
}
