﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class SellerNoteRepository : ISellerNoteRepository
    {
        ResailingDBContext context = new ResailingDBContext();

        public IEnumerable<SellerNote> SellerNote
        {
            get
            {
                return context.SellerNotes;
            }
        }
    }
}
