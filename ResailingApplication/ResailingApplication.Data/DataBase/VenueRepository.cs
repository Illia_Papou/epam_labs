﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class VenueRepository : IVenueRepository
    {
        //ResailingDBContext context = new ResailingDBContext();
        private ResailingDBContext _context;

        public VenueRepository()
        {
            _context = new ResailingDBContext();
        }

        public VenueRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Venue> Venue
        {
            get
            {
                return _context.Venues
                    .Include(v => v.City);
            }
        }

        public void Adding(Venue venue)
        {
            _context.Venues.Add(venue);
            _context.SaveChanges();
        }

        public void Updating(int index, Venue venue)
        {
            //_context.Venues.Remove(venue);
            _context.Venues.ToList().Insert(index, venue);
            _context.SaveChanges();
        }

        public void Removing(Venue venue)
        {
            _context.Venues.Remove(venue);
            _context.SaveChanges();
        }
    }
}
