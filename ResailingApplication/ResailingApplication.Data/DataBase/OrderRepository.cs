﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class OrderRepository : IOrderRepository
    {
        // ResailingDBContext context = new ResailingDBContext();
        private ResailingDBContext _context;

        public OrderRepository()
        {
            _context = new ResailingDBContext();
        }

        public OrderRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Order> Order
        {
            get
            {
                return _context.Orders
                    .Include(b => b.Buyer)
                    .Include(t => t.Ticket)
                        .ThenInclude(e => e.Event.Venue.City)
                    .Include(t => t.Ticket)
                        .ThenInclude(s => s.Seller)
                        .Include(s => s.StatusOrder)
                    .Include(t => t.Ticket.SellerNote);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Adding(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
        }

        public void Updating(int index, Order order)
        {
            //_context.Users.Remove(user);
            _context.Orders.ToList().Insert(index, order);
            _context.SaveChanges();
        }

        public void Removing(Order order)
        {
            _context.Orders.Remove(order);
            _context.SaveChanges();
        }
    }
}
