﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class UserRepository : IUserRepository
    {
        //ResailingDBContext context = new ResailingDBContext();

        private ResailingDBContext _context;

        public UserRepository()
        {
            _context = new ResailingDBContext();
        }

        public UserRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<User> User
        {
            get
            {
                return _context.Users
                    .Include(u => u.Role);
            }
        }

        public void Adding(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void Updating(int index, User user)
        {
            //_context.Users.Remove(user);
            _context.Users.ToList().Insert(index, user);
            _context.SaveChanges();
        }

        public void Removing(User user)
        {
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}
