﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class RoleRepositorys : IRoleRepository
    {
        //ResailingDBContext context = new ResailingDBContext();

        private ResailingDBContext _context;

        public RoleRepositorys()
        {
            _context = new ResailingDBContext();
        }

        public RoleRepositorys(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Role> Role
        {
            get
            {
                return _context.Roles;
            }
        }

        public void Adding(Role role)
        {
            _context.Roles.Add(role);
            _context.SaveChanges();
        }

        public void Updating(int index, Role role)
        {
            _context.Roles.Remove(role);
            _context.Roles.ToList().Insert(index, role);
            _context.SaveChanges();
        }

        public void Removing(Role role)
        {
            _context.Roles.Remove(role);
            _context.SaveChanges();
        }
    }
}
