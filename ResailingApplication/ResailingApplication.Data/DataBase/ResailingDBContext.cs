﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class ResailingDBContext : DbContext
    {
        public DbSet<City> Cities { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Venue> Venues { get; set; }
        public DbSet<SellerNote> SellerNotes { get; set; }
        public DbSet<StatusOrder> StatusOrders { get; set; }



        public ResailingDBContext()
        {

        }

        
        public ResailingDBContext(DbContextOptions<ResailingDBContext> contextOptions)
            :base(contextOptions)
        {

        }
    }
}
