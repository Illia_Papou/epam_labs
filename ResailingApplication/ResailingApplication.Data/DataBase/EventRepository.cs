﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class EventRepository : IEventRepository
    {
        //ResailingDBContext context = new ResailingDBContext();
        private ResailingDBContext _context;

        public EventRepository()
        {
            _context = new ResailingDBContext();
        }

        public EventRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Event> Event
        {
            get
            {
                return _context.Events.Include(e => e.Venue.City);
            }
        }

        public void Adding(Event evente)
        {
            _context.Events.Add(evente);
            _context.SaveChanges();
        }

        public void Updating(int index, Event evente)
        {
            //_context.Events.Remove(evente);
            _context.Events.ToList().Insert(index, evente);
            _context.SaveChanges();
        }

        public void Removing(Event evente)
        {
            _context.Events.Remove(evente);
            _context.SaveChanges();
        }
    }
}
