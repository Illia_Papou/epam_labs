﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class CityRepository : ICityRepository
    {
        private ResailingDBContext _context;

        public CityRepository()
        {
            _context = new ResailingDBContext();
        }

        public CityRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<City> City
        {
            get
            {
                return _context.Cities
                    .Include(v => v.Venues);
            }
        }

        public void Adding(City city)
        {
            _context.Cities.Add(city);
            _context.SaveChanges();
        }

        public void Updating(int index, City city)
        {
            //_context.Cities.Remove(city);
            _context.Cities.ToList().Insert(index, city);
            _context.SaveChanges();
        }

        public void Removing(City city)
        {
            _context.Cities.Remove(city);
            _context.SaveChanges();
        }
    }
}
