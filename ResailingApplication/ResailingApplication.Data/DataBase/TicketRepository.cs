﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Business.Models;

namespace ResailingApplication.Data.DataBase
{
    public class TicketRepository : ITicketRepository
    {
        //ResailingDBContext context = new ResailingDBContext();
        private ResailingDBContext _context;

        public TicketRepository()
        {
            _context = new ResailingDBContext();
        }

        public TicketRepository(ResailingDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Ticket> Ticket
        {
            get
            {
                return _context.Tickets
                    .Include(t => t.Event)
                        .ThenInclude(e => e.Venue)
                            .ThenInclude(c => c.City)
                    .Include(s => s.Seller)
                    .Include(s => s.SellerNote);
            }
        }

        public void Adding(Ticket ticket)
        {
            _context.Tickets.Add(ticket);
            _context.SaveChanges();
        }

        public void Updating(int index, Ticket ticket)
        {
            //_context.Users.Remove(user);
            _context.Tickets.ToList().Insert(index, ticket);
            _context.SaveChanges();
        }

        public void Removing(Ticket ticket)
        {
            _context.Tickets.Remove(ticket);
            _context.SaveChanges();
        }
    }
}
