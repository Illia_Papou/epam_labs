﻿using ResailingApplication.Business.Models;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Data.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services
{
    public class TicketService : ITicketService
    {
        private ITicketRepository _ticketRepository;

        public TicketService(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        public List<Ticket> GetAllTickets()
        {
            return _ticketRepository.Ticket.ToList();
        }

        public Ticket GetTicketById(int id)
        {
            return _ticketRepository.Ticket.ToList().FirstOrDefault(x => x.Id == id);
        }

        public List<Ticket> GetTicketsByEevent(int eventId)
        {
            return _ticketRepository.Ticket.Where(x => x.Event.Id == eventId).ToList();
        }

        public List<Ticket> GetTicketsByStatus(SellerNote note)
        {
            return _ticketRepository.Ticket.Where(t => t.SellerNote == note).ToList();
        }

        public List<Ticket> GetTicketsByUser(User user)
        {
            throw new NotImplementedException();
        }
        public void CreateTicket(Ticket ticket)
        {
            //ticket.Id = _ticketRepository.Ticket.ToList().Count + 1;
            _ticketRepository.Adding(ticket);
        }
        public void UpdateTicket(Ticket ticket)
        {
            int index = _ticketRepository.Ticket.ToList().FindIndex(c => c.Id == ticket.Id);
            if (index >= 0)
            {
                //_ticketRepository.Ticket.ToList().RemoveAt(index);
                _ticketRepository.Updating(index, ticket);
            }
        }
        public void DeleteTicket(int id)
        {
            Ticket ticket = _ticketRepository.Ticket.ToList().Find(c => c.Id == id);
            if (ticket != null)
                _ticketRepository.Removing(ticket);
        }
        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
