﻿using ResailingApplication.Business.Models;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Data.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private IRoleRepository _roleRepository;

        public UserService(IUserRepository userRepository, IRoleRepository roleRepository)
        {
            _userRepository = userRepository;
            _roleRepository = roleRepository;
        }
        public List<User> GetAllUsers()
        {
            return _userRepository.User.ToList();
        }
        public User GetUser(int id)
        {
            return _userRepository.User.ToList().FirstOrDefault(item => item.Id == id);
        }

        public void CreateUser(User user)
        {
            //user.Id = _userRepository.User.ToList().Count + 1;
            _userRepository.Adding(user);
        }
        public void UpdateUser(User user)
        {
            //int index = _userRepository.User.ToList().FindIndex(c => c.Id == user.Id);
            if (user.Id >= 0)
            {
                _userRepository.Updating(user.Id, user);
                //_userRepository.User.ToList().Insert(index, user);
            }
        }
        public void DeleteUser(int id)
        {
            User user = _userRepository.User.ToList().Find(c => c.Id == id);
            if (user != null)
                _userRepository.Removing(user);
        }
        public List<Role> GetAllRoles()
        {
            return _roleRepository.Role.ToList();
        }
        public Role GetRole(int id)
        {
            return _roleRepository.Role.ToList().FirstOrDefault(item => item.Id == id);
        }
        public void CreateRole(Role role)
        {
            //role.Id = _roleRepository.Role.ToList().Count + 1;
            _roleRepository.Adding(role);
        }
        public void UpdateRole(Role role)
        {
            int index = _roleRepository.Role.ToList().FindIndex(c => c.Id == role.Id);
            if (index >= 0)
            {
                //_roleRepository.Role.ToList().RemoveAt(index);
                _roleRepository.Updating(index, role);
            }
        }
        public void DeleteRole(int id)
        {
            Role role = _roleRepository.Role.ToList().Find(c => c.Id == id);
            if (role != null)
                _roleRepository.Removing(role);
        }
        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
