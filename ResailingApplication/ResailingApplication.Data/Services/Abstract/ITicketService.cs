﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services.Abstract
{
    public interface ITicketService
    {
        List<Ticket> GetAllTickets();
        List<Ticket> GetTicketsByEevent(int eventId);
        List<Ticket> GetTicketsByUser(User user);
        Ticket GetTicketById(int Id);
        List<Ticket> GetTicketsByStatus(SellerNote note);
        void CreateTicket(Ticket ticket);
        void UpdateTicket(Ticket ticket);
        void DeleteTicket(int id);
        void Save();
    }
}
