﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services.Abstract
{
    public interface IEventService
    {
        List<Event> GetAllEvents();
        Event GetEvent(int id);
        void CreateEvent(Event evente);
        void UpdateEvent(Event evente);
        void DeleteEvent(int id);
        List<City> GetAllCity();
        City GetCity(int id);
        void CreateCity(City city);
        void UpdateCity(City city);
        void DeleteCity(int id);
        List<Venue> GetAllVenue();
        Venue GetVenue(int id);
        void CreateVenue(Venue venue);
        void UpdateVenue(Venue venue);
        void DeleteVenue(int id);
        void Save();
    }
}
