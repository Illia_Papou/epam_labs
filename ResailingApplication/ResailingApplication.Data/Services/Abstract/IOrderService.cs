﻿using ResailingApplication.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services.Abstract
{
    public interface IOrderService
    {
        Order GetOrdersById(int id);
        List<Order> GetAllOrders();
        List<StatusOrder> GetAllStatusOrders();
        void CreateOrder(Order order);
        void UpdateOrder(Order order);
        void DeleteOrder(int id);
        void Save();
    }
}
