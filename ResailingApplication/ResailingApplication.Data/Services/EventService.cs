﻿using ResailingApplication.Business.Models;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Data.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services
{
    public class EventService : IEventService
    {
        private IEventRepository _eventRepository;
        private ICityRepository _cityRepository;
        private IVenueRepository _venueRepository;

        public EventService(IEventRepository eventRepository, ICityRepository cityRepository, IVenueRepository venueRepository)
        {
            _eventRepository = eventRepository;
            _cityRepository = cityRepository;
            _venueRepository = venueRepository;
        }

        public List<Event> GetAllEvents()
        {
            return _eventRepository.Event.ToList();
        }

        public Event GetEvent(int id)
        {
            return _eventRepository.Event.ToList().FirstOrDefault(item => item.Id == id);
        }

        public void CreateEvent(Event evente)
        {
            //evente.Id = _eventRepository.Event.ToList().Count + 1;
            _eventRepository.Adding(evente);
        }
        public void UpdateEvent(Event evente)
        {
            int index = _eventRepository.Event.ToList().FindIndex(c => c.Id == evente.Id);
            if (index >= 0)
            {
                //_eventRepository.Event.ToList().RemoveAt(index);
                _eventRepository.Updating(index, evente);
            }
        }

        public void DeleteEvent(int id)
        {
            Event evente = _eventRepository.Event.ToList().Find(c => c.Id == id);
            if (evente != null)
                _eventRepository.Removing(evente);
        }

        public List<City> GetAllCity()
        {
            return _cityRepository.City.ToList();
        }
        public City GetCity(int id)
        {
            return _cityRepository.City.FirstOrDefault(item => item.Id == id);
        }

        public void CreateCity(City city)
        {
            //city.Id = _cityRepository.City.ToList().Count + 1;
            _cityRepository.Adding(city);
        }
        public void UpdateCity(City city)
        {
            int index = _cityRepository.City.ToList().FindIndex(c => c.Id == city.Id);
            if (index >= 0)
            {
                //_cityRepository.City.ToList().RemoveAt(index);
                _cityRepository.Updating(index, city);
            }
        }

        public void DeleteCity(int id)
        {
            City city = _cityRepository.City.ToList().Find(c => c.Id == id);
            if (city != null)
                _cityRepository.Removing(city);
        }

        public List<Venue> GetAllVenue()
        {
            return _venueRepository.Venue.ToList();
        }
        public Venue GetVenue(int id)
        {
            return _venueRepository.Venue.FirstOrDefault(item => item.Id == id);
        }

        public void CreateVenue(Venue venue)
        {
            //venue.Id = _venueRepository.Venue.ToList().Count + 1;
            _venueRepository.Adding(venue);
        }

        public void UpdateVenue(Venue venue)
        {
            int index = _venueRepository.Venue.ToList().FindIndex(c => c.Id == venue.Id);
            if (index >= 0)
            {
                //_venueRepository.Venue.ToList().RemoveAt(index);
                _venueRepository.Updating(index, venue);
            }
        }

        public void DeleteVenue(int id)
        {
            Venue venue = _venueRepository.Venue.ToList().Find(c => c.Id == id);
            if (venue != null)
                _venueRepository.Removing(venue);            
        }
        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
