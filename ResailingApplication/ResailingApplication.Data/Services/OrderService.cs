﻿using ResailingApplication.Business.Models;
using ResailingApplication.Data.Interfaces;
using ResailingApplication.Data.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResailingApplication.Data.Services
{
    public class OrderService: IOrderService
    {
        private IOrderRepository _orderRepository;
        private IStatusOrderRepository _statusOrderRepository;

        public OrderService(IOrderRepository orderRepository, IStatusOrderRepository statusOrderRepository)
        {
            _orderRepository = orderRepository;
            _statusOrderRepository = statusOrderRepository;
        }
        public List<Order> GetAllOrders()
        {
            return _orderRepository.Order.ToList();
        }

        public Order GetOrdersById(int id)
        {
            return _orderRepository.Order.ToList().FirstOrDefault(u => u.Id == id);
        }

        public void CreateOrder(Order order)
        {
            //order.Id = _orderRepository.Order.ToList().Count + 1;
            _orderRepository.Adding(order);
        }
        public void UpdateOrder(Order order)
        {
            //int index = _orderRepository.Order.ToList().FindIndex(c => c.Id == order.Id);
            if (order.Id >= 0)
            {
                //_orderRepository.Order.ToList().RemoveAt(index);
                _orderRepository.Updating(order.Id, order);
            }
        }
        public void DeleteOrder(int id)
        {
            Order order = _orderRepository.Order.ToList().Find(c => c.Id == id);
            if (order != null)
                _orderRepository.Removing(order);
        }

        public List<StatusOrder> GetAllStatusOrders()
        {
            return _statusOrderRepository.StatusOrder.ToList();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
